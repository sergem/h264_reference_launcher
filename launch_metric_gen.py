#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

name="Poznan_Hall2_00"
length = 200
view0       = 6
view1       = 7
view_synth  = 6.5
depth_nearest = -23.394160
depth_furthest = -172.531931
w = 1920
h = 1088

lines = []
#qp_set = [ 15, 20, 25, 30, 35, 40 ]
#qp_set = [ 10, 30, 35, 40 ]
qp_set = [ 10, 15, 20, 25, 30, 35, 40, 45 ]

for qp in qp_set:
    qd = qp
    lines += [ 
         "..\\dp\\bin\\msu_metric.exe -f synth\\{name}_{w}x{h}_synth_cam_{view_synth}_q0.yuv IYUV -yw {w} -yh {h} -f synth\\{name}_{w}x{h}_synth_qp{qp}_qd{qd}_cam_{view_synth}.yuv IYUV -yw {w} -yh {h} -metr apsnr -cc YYUV -sc 1 -af2 metric2_apsnr.log\n".format( qp=qp, qd=qd, view0=view0, view1=view1, view_synth=view_synth, name=name, length=length, depth_nearest=depth_nearest, depth_furthest=depth_furthest, w=w, h=h ) ]

f = open( "asdasD", "w" )
f.writelines( lines );
f.close()

