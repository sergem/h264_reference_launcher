#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

import os
import glob
import sys
import shutil
import subprocess
import argparse 
import create_config_codec

###############################################################################
isTest = False
silent = 0
###############################################################################

def Execute( command ):
    result = 0
    if silent < 2: 
        sys.stdout.flush()  
        print 
        print( "::EXECUTE:" )
        print( command )
        sys.stdout.flush()
        sys.stderr.flush()
    if not isTest:
        result = subprocess.call( command, shell=True )
        print "result=", result
        if result != 0:
            raise Exception( "Execution failed. Error code = " + str( result ) + ". Current command: " + command )
    return result
###############################################################################
def PrintUsage():
    print "Usage:"
    print sys.argv[0] + " <template> <parameters>"
###############################################################################
def MakeDirIfNotExists( pathDir ):
    if( os.path.isdir( pathDir ) == False ):
        os.makedirs( pathDir )
###############################################################################
def WindowsPath( path ):
    return os.path.normpath(path).replace( "/", "\\\\" )
###############################################################################
###############################################################################
###############################################################################
def Main( argv ):

    ###############################################################################
    parser = argparse.ArgumentParser(
        description='VSRS launcher',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=
        'Usage example:\n'
        '     ./synth_launch.py \\\n'
        '           ../sources/Poznan_Street_00_1920x1088_rec_cam05.yuv \\\n'
        '           ../sources/Poznan_Street_00_1920x1088_rec_cam04.yuv \\ \n'
        '           ../sources/Poznan_Street_00_1920x1088_depth_cam05.yuv \\ \n'  
        '           ../sources/Poznan_Street_00_1920x1088_depth_cam04.yuv \\ \n'
        '           ../sources/Poznan_Street_00_1920x1088_reg.txt \\ \n'
        '           param_cam5 param_cam4 param_cam4.5 \\ \n'
        '           synth/Poznan_Street_00_1920x1088_synth_cam04.5.yuv \\ \n'
        '           1920 1088 5 -34.506386 -2760.510889 \n'  
        '\n'
        '      ./synth_launch.py ../sources/Poznan_Street_00_1920x1088_rec_cam_4.yuv ../sources/Poznan_Street_00_1920x1088_rec_cam_3.yuv ../sources/Poznan_Street_00_1920x1088_depth_cam_4.yuv ../sources/Poznan_Street_00_1920x1088_depth_cam_3.yuv ../sources/Poznan_Street_00_1920x1088_reg.txt param_cam4 param_cam3 param_cam3.5 synth/Poznan_Street_00_1920x1088_synth_cam03.5.yuv 1920 1088 5 -34.506386 -2760.510889\n'  
        '')
    parser.add_argument('LeftFrm', action='store', type=str )
    parser.add_argument('RightFrm', action='store', type=str )
    parser.add_argument('LeftDpt', action='store', type=str )
    parser.add_argument('RightDpt', action='store', type=str )

    parser.add_argument('CameraParameterFile', action='store', type=str )
        
    parser.add_argument('LeftCameraName', action='store', type=str )
    parser.add_argument('RightCameraName', action='store', type=str )
    
    parser.add_argument('VirtualCameraName', action='store', type=str )
    parser.add_argument('Output', action='store', type=str )
    
    
    parser.add_argument('width', action='store', type=int )
    parser.add_argument('height', action='store', type=int )
    parser.add_argument('length', action='store', type=int )
    
    parser.add_argument('NearestDepthValue', action='store', type=float )
    parser.add_argument('FarthestDepthValue', action='store', type=float )
    
    #parser.add_argument('--log', action='store', default = "log.txt" )

    params = vars( parser.parse_args( argv[1:] ) )
   
    print params
    print "asd"
    #if( params['show_parameters'] ):
    #    return
    ###############################################################################
        
    pathBin = os.path.abspath("../Bin/VSRS3.5/ViewSynVC8.exe")
     
    #pathBinCreateConfig = os.path.abspath("./create_config_codec.py")
    
    pathConfigTemplate = os.path.normpath( "config_synth_template.cfg" )
    
    
    
    MakeDirIfNotExists( os.path.dirname(params['Output']) )
    
    
    
    pathConfigOutput    = params['Output'] + ".cfg"
    
    ###################### 
    
    arglist = [ 
                        #pathBinCreateConfig, 
                        pathConfigTemplate, 
                        pathConfigOutput, 
                        "LeftViewImageName="            + WindowsPath(params['LeftFrm']),
                        "RightViewImageName="           + WindowsPath(params['RightFrm']),
                        "LeftDepthMapName="             + WindowsPath(params['LeftDpt'] ),
                        "RightDepthMapName="            + WindowsPath(params['RightDpt']),
                        
                        "SourceWidth=%d" % params['width'],
                        "SourceHeight=%d" % params['height'],
                        "TotalNumberOfFrames=%d" % params['length'],
                        
                        "LeftNearestDepthValue=%Lf"     % params['NearestDepthValue'],
                        "LeftFarthestDepthValue=%Lf"    % params['FarthestDepthValue'],
                        
                        "RightNearestDepthValue=%Lf"    % params['NearestDepthValue'],
                        "RightFarthestDepthValue=%Lf"   % params['FarthestDepthValue'],
                        
                        "CameraParameterFile="          + WindowsPath(params['CameraParameterFile']),
                        'LeftCameraName='               + params['LeftCameraName'],
                        'RightCameraName='              + params['RightCameraName'],
                        'VirtualCameraName='            + params['VirtualCameraName'],
                        
                        'OutputVirtualViewImageName='   + WindowsPath(params['Output'])
                        ]
    command = " ".join(arglist)
    print "111"
    
    print "122"
    print command
    create_config_codec.Main( arglist )
    #Execute( command )
    print ( "===============================" )
    ######################
    command2 = " ".join([ 
                        pathBin, 
                        pathConfigOutput, 
                        ]) 
    
    Execute( command2 )
        
        
###############################################################################

if __name__ == '__main__':
    #try:
        Main( sys.argv )
    #except Exception as e:
    #    print e
        