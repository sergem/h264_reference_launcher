#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 12:14 25.04.2013
#
###############################################################################

name="Poznan_Hall2_00"
length = 200
view0       = 5
view1       = 7
view_synth  = 6
depth_nearest = -23.394160
depth_furthest = -172.531931
w = 1920
h = 1088
#type_video = "depth"

lines = []
qp_set = [ 15, 20, 25, 30, 35, 40 ]

for type_video in [ "rec", "depth" ]:
    for qp in qp_set:
        qd = qp
        lines += [ 
            "start python codec_launch.py {name} {type_video} {w} {h} {length} {qp} --views {view0} {view1} > {name}_codec_views_{view0}_{view1}_{type_video}_{qp}.log\n\r".format( qp=qp, qd=qd, view0=view0, view1=view1, view_synth=view_synth, name=name, length=length, depth_nearest=depth_nearest, depth_furthest=depth_furthest, w=w, h=h, type_video=type_video ) ]

f = open( "asdasD", "w" )
f.writelines( lines );
f.close()