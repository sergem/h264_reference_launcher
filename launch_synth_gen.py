#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

name="h2"
length = 11
view0       = 5
view1       = 7
view_synth  = 6
depth_nearest = -23.394160
depth_furthest = -172.531931
w = 1920
h = 1088

lines = []
qp_set = [ 15, 20, 25, 30, 35, 40 ]
#qp_set = [ 10, 30, 35, 40 ]
#qp_set = [ 15, 20, 25 ]

for qp in qp_set:
    qd = qp
    lines += [ 
        "start python synth_launch.py Recon\{name}_{w}x{h}_rec_q{qp}.000000_cam_{view1}.yuv " 
        "Recon\{name}_{w}x{h}_rec_q{qp}.000000_cam_{view0}.yuv "
        "Recon\{name}_{w}x{h}_depth_q{qd}.000000_cam_{view1}.yuv "
        "Recon\{name}_{w}x{h}_depth_q{qd}.000000_cam_{view0}.yuv "
        "..\sources\{name}_{w}x{h}_reg.txt "
        "param_cam{view1} param_cam{view0} param_cam{view_synth} "
        "synth\{name}_{w}x{h}_synth_qp{qp}_qd{qd}_cam_{view_synth}.yuv "
        "{w} {h} {length} {depth_nearest} {depth_furthest} > synth_log_qp{qp}_qd{qd}.log\n\r".format( qp=qp, qd=qd, view0=view0, view1=view1, view_synth=view_synth, name=name, length=length, depth_nearest=depth_nearest, depth_furthest=depth_furthest, w=w, h=h ) ]

f = open( "asdasD", "w" )
f.writelines( lines );
f.close()