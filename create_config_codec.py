#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

import os
import glob
import sys
import shutil
import re

###############################################################################
def PrintUsage():
    print "Usage:"
    print sys.argv[0] + " <template> <parameters>"
###############################################################################

def UpdateParameter( text, param ):
    match1 = re.match( r"(.+)=(.+)", param )
    if match1 is None or match1.lastindex != 2:
        raise Exception( "Failed to parse parameter " + param )
    
    name = match1.group(1)
    value = match1.group(2)
    cnt = 0
    for i in range( len( text ) ) :
        line = text[i]
        if re.search( name + "\s+\S+", line ) is None:
            continue
        text[i] = name + "    " + value + "\n"
        cnt += 1
        if cnt > 1:
            raise Exception( "Multiple occurencies of parameter %s were found in template. Unsupported case." % name )

###############################################################################
###############################################################################
def Main( arguments ):
    idFirstSpareParam = 2
    
    pathConfigTemplate = os.path.normpath( arguments[0] )
    pathConfigOutput = os.path.normpath( arguments[1] )
    
    fin = open( pathConfigTemplate, "r" );
    textTemplate = fin.readlines()
    fin.close()
    
    text = textTemplate
    text += [ "\n#########################################\n" ];
    text += [ "# Following parameters overrides previously defined values\n" ];
    
    for i in range( idFirstSpareParam, len(arguments) ):
        UpdateParameter( text, arguments[i] )
    
    
    f = open( pathConfigOutput, "w" );
    f.writelines( text ); 
    f.close()


###############################################################################


if __name__ == '__main__':
    Main( sys.argv[1:] )

