#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

import os
import glob
import sys
import shutil

###############################################################################
def PrintUsage():
    print "Usage:"
    print sys.argv[0] + " <pathToConfig> <output file> <view0> <view1>"
###############################################################################

numArg = 5
if len( sys.argv ) < numArg:
    print "Error: Not enough parameters"
    PrintUsage(); 
    exit()

if len( sys.argv ) > numArg:
    print "Error: Too many arguments"
    PrintUsage(); 
    exit()

###############################################################################

        
###############################################################################
###############################################################################

text = """
#====================== Assembler: View Encode order ==========================
OutputFile              %s
NumberOfViews           2
InputFile0              %s 
InputFile1              %s
""" % ( sys.argv[2], sys.argv[3], sys.argv[4] )

f = open( sys.argv[1], "w" );
f.writelines( text ); 
f.close()