#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

name="Poznan_Hall2_00"
w = 1920
h = 1088
length = 200
view0       = 6
view1       = 7
view_synth  = 6.5
depth_nearest = -23.394160
depth_furthest = -172.531931

qJPEG_set = [ 256, ]
scale = 4
cor = 0
interval = 10

lines = []
qp_set = [ 15, 20, 25, 30, 35, 40 ]
#qp_set = [ 10, 30, 35, 40 ]
#qp_set = [ 15, 20, 25 ]
qp_set = [ 20, ]

for qJPEG in qJPEG_set:
    for qp in qp_set:
        qd = qp

        lines += [ 
            "python synth_launch.py "
            "Recon\{name}_{w}x{h}_rec_q{qp}.000000_cam_{view1}.yuv " 
            "Recon\{name}_{w}x{h}_rec_q{qp}.000000_cam_{view0}.yuv "
            "..\dp\dp\{name}_{w}x{h}_depth_cam_{view1}_qp{qp}_i{interval}_q{qJPEG}_scale{scale}_cor{cor}__ucr_dudp_.yuv "
            "..\dp\dp\{name}_{w}x{h}_depth_cam_{view0}_qp{qp}_i{interval}_q{qJPEG}_scale{scale}_cor{cor}__ucr_dudp_.yuv "
            "..\sources\{name}_{w}x{h}_reg.txt "
            "param_cam{view1} param_cam{view0} param_cam{view_synth} "
            "synth\{name}_{w}x{h}_synth_cam_{view_synth}_qp{qp}_i{interval}_q{qJPEG}_scale{scale}_cor{cor}.yuv "
            "{w} {h} {length} {depth_nearest} {depth_furthest} > synth_log_qp{qp}_i{interval}_q{qJPEG}_scale{scale}_cor{cor}.log\n"
            .format( qp=qp, qd=qd, view0=view0, view1=view1, view_synth=view_synth, name=name, length=length, depth_nearest=depth_nearest, depth_furthest=depth_furthest, w=w, h=h, qJPEG = qJPEG, interval=interval, cor=cor, scale=scale ) ]


        lines += [ 
            "..\\dp\\bin\\msu_metric.exe"
            " -f synth\\{name}_{w}x{h}_synth_cam_{view_synth}_q0.yuv"
            " IYUV -yw {w} -yh {h}"
            " -f synth\\{name}_{w}x{h}_synth_cam_{view_synth}_qp{qp}_i{interval}_q{qJPEG}_scale{scale}_cor{cor}.yuv"
            " IYUV -yw {w} -yh {h}"
            " -metr apsnr -cc YYUV -sc 1 -af2 metric2_apsnr.log\n".format( qp=qp, qd=qd, view0=view0, view1=view1, view_synth=view_synth, name=name, length=length, depth_nearest=depth_nearest, depth_furthest=depth_furthest, w=w, h=h, qJPEG = qJPEG, interval=interval, cor=cor, scale=scale ) ]

        lines += [ "\n" ]

f = open( "asdasD", "w" )
f.writelines( lines );
f.close()

