#!/bin/python
# -*- coding: cp1251 -*-

###############################################################################
# Sergey Matyunin
# 2013 02 28
#
###############################################################################

import os
import glob
import sys
import shutil
import subprocess
import argparse 

###############################################################################
isTest = False
silent = 0
###############################################################################

def Execute( command ):
    result = 0
    if silent < 2: 
        sys.stdout.flush()  
        print 
        print( "::EXECUTE:" )
        print( command )
        sys.stdout.flush()
        sys.stderr.flush()
    if not isTest:
        result = subprocess.call( command, shell=True )
        print "result=", result
        if result != 0:
            raise Exception( "Execution failed. Error code = " + str( result ) + ". Current command: " + command )
    return result
###############################################################################
def PrintUsage():
    print "Usage:"
    print sys.argv[0] + " <template> <parameters>"
###############################################################################
def MakeDirIfNotExists( pathDir ):
    if( os.path.isdir( pathDir ) == False ):
        os.makedirs( pathDir )
###############################################################################

###############################################################################
###############################################################################
###############################################################################
def Main( argv ):

    ###############################################################################
    parser = argparse.ArgumentParser(
        description='JMVC launcher',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=
        'Usage example:\n'
        '    python codec_launch.py Poznan_Street_00 rec 1920 1088 250 31 --views 3 4\n'  
        'process files Poznan_Street_00_1920x1088_rec_cam_3.yuv and Poznan_Street_00_1920x1088_rec_cam_4.yuv\n')
    parser.add_argument('name', action='store', type=str )
    parser.add_argument('postfix', action='store', type=str )
    parser.add_argument('width', action='store', type=int )
    parser.add_argument('height', action='store', type=int )
    parser.add_argument('length', action='store', type=int )
    parser.add_argument('quality', action='store', type=float )
    parser.add_argument('--views', action='store', type=int, nargs=2, default=[3,4] )
    parser.add_argument('--log', action='store', default = "log.txt" )
    

    params = vars( parser.parse_args( argv[1:] ) )
   
    name = str( params['name'] )
    postfixType = str( params['postfix'] )
    w = int( params['width'] )
    h = int( params['height'] )
    len = int( params['length'] )
    q = float( params['quality'] )
    view0 = int( params['views'][0] )
    view1 = int( params['views'][1] )
    
        
    #print params
    #return
    #if( params['show_parameters'] ):
    #    return
    ###############################################################################
        
    pathBin = os.path.abspath("../Bin/JMVC8.3.1/H264AVCEncoderLibTestStatic.exe")
    pathBinAsm = os.path.abspath("../Bin/JMVC8.3.1/MVCBitStreamAssemblerStatic.exe")
    
    pathBinCreateConfig = os.path.abspath("./create_config_codec.py")
    pathBinCreateConfigAsm = os.path.abspath("./create_config_asm.py")
    
    pathConfigTemplate = os.path.normpath( "config_codec_template.cfg" )
    
    
        
    pathDirBit = "Bit"
    pathDirDecoded = "Recon"
    pathDirMotion = "Mot"
    pathDirConfig = "Config"
    
    
    MakeDirIfNotExists( pathDirBit )
    MakeDirIfNotExists( pathDirDecoded )
    MakeDirIfNotExists( pathDirMotion )
    MakeDirIfNotExists( pathDirConfig )
    
    
    prefixName = "%s_%dx%d_%s" % ( name, w, h, postfixType )
    prefixNameQuality = "%s_q%Lf" % ( prefixName, q )
    
    pathConfigOutput    = pathDirConfig + "\\" + prefixNameQuality + ".cfg"
    pathConfigOutputAsm = pathDirConfig + "\\" + prefixNameQuality + "_asm.cfg"
    
    ###################### 

    ftmpl = open( pathConfigTemplate, "r" )
    tmpl = ftmpl.read()
    ftmpl.close()
    config = tmpl.format( 
        InputFile="..\sources\%s_cam"           % ( prefixName, ),
        OutputFile="%s\\%s_tbit_cam"            % ( pathDirBit,       prefixNameQuality ),
        ReconFile="%s\\%s_cam"                  % ( pathDirDecoded,   prefixNameQuality ),
        MotionFile="%s\\%s_mot_cam"             % ( pathDirConfig,    prefixNameQuality ),
        SourceWidth=w,
        SourceHeight=h,
        FramesToBeEncoded=len,
        BasisQP=q,
        NumViewsMinusOne=1,
        view0=view0,
        view1=view1
        )

    fconfig = open( pathConfigOutput, "w" )
    fconfig.writelines(config)
    fconfig.close()
    
    print ( "===============================" )
    ######################
    command2 = " ".join([ 
                        pathBin, 
                        "-vf", 
                        pathConfigOutput, 
                        str(view0) ]) 
    
    Execute( command2 )
    print ( "===============================" )
    ##########################
    command3 = " ".join([ 
                        pathBin, 
                        "-vf", 
                        pathConfigOutput, 
                        str(view1) ])
     
    Execute( command3 )
    print ( "===============================" )
    ##########################
    command4 = " ".join([ 
                        pathBinCreateConfigAsm, 
                        pathConfigOutputAsm,
                        "%s\\%s_tbit_cam.264"              % ( pathDirBit, prefixNameQuality ), 
                        "%s\\%s_tbit_cam_%d.264"           % ( pathDirBit, prefixNameQuality, view0),
                        "%s\\%s_tbit_cam_%d.264"           % ( pathDirBit, prefixNameQuality, view1)
                         ])
    
    Execute( command4 )
    print ( "===============================" )
    
    ######################
    command5 = " ".join([ 
                        pathBinAsm, 
                        "-vf", 
                        pathConfigOutputAsm
                        ]) 
    
    Execute( command5 )
    print ( "===============================" )
    
        
###############################################################################

if __name__ == '__main__':
    try:
        Main( sys.argv )
    except Exception as e:
        print e
        